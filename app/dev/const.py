
POINT_DATA = {
   "agv2" : {
        "18": {
            "uid" : 2,
            "rfid" : "181818",
            "route_id" : 6,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1000
        },
        "19": {
            "uid" : 2,
            "rfid" : "191919",
            "route_id" : 6,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1001
        },
        "10": {
            "uid" : 2,
            "rfid" : "78765017",
            "route_id" : 6,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1002
        },
   },
    "agv1" : {
        "8": {
            "uid" : 1,
            "rfid" : "78812247",
            "route_id" : 4,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1000
        },
        "9": {
            "uid" : 1,
            "rfid" : "78775377",
            "route_id" : 4,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1001
        },
        "10": {
            "uid" : 1,
            "rfid" : "78765017",
            "route_id" : 4,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1002
        },
   },
   "agv3" : {
        "8": {
            "uid" : 3,
            "rfid" : "78812247",
            "route_id" : 4,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1000
        },
        "9": {
            "uid" : 3,
            "rfid" : "78775377",
            "route_id" : 4,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1001
        },
        "10": {
            "uid" : 3,
            "rfid" : "78765017",
            "route_id" : 4,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1002
        },
        "19": {
            "uid" : 3,
            "rfid" : "191919",
            "route_id" : 4,
            "status" : "MOVING",
            "command" : "",
            "battery" : 99,
            "runningtime" : 1002
        },
   }
}
